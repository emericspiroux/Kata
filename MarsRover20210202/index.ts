import Grid from './src/classes/Grid';
import MarsRover from './src/classes/MarsRover';

const height = 10;
const width = 10;

const grid = new Grid({ height, width }, { orientation: 'N', x: 0, y: height - 1 });
grid.print();

const tests = ['M', 'MMMMMMMMMM'];
const expectedResult = ['0:8:N', '0:9:N', '3:6:E', '6:9:S'];

for (const [index, test] of tests.entries()) {
  console.log('Commands:', test);
  const marsRover = new MarsRover(grid);
  const result = marsRover.execute(test);
  console.log('Result:', result, '\t Match expectations:', result === expectedResult[index] ? '✅' : '❌');
}
