type TPosition = { x: number; y: number; orientation: TOrientation };

export default TPosition;
