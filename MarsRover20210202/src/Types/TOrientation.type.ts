type TOrientation = 'N' | 'E' | 'S' | 'W';

export default TOrientation;
