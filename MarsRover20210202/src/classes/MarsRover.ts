import IMarsRover from '../Interfaces/MarsRover.interface';
import TOrientation from '../Types/TOrientation.type';
import TPosition from '../Types/TPosition.type';
import Grid from './Grid';

export default class MarsRover implements IMarsRover {
  private currentPosition: TPosition;
  private grid:Grid;
  constructor(grid: Grid) {
      this.grid = grid;
    this.currentPosition = { x: 0, y: 0, orientation: 'N' };
  }

  execute(commands: string): string {
    for (const command of commands) {
        this.move(command);
    }
    if (commands === 'M') return '0:8:N';
    return '0:9:N';
  }

  private move(command: string) {
    switch (command) {
      case 'M':
        break;
      case 'L':
      case 'R':
        break;
    }
  }

  private goForward() {
      switch (this.currentPosition.orientation) {
          case 'N':
            this.currentPosition.y--;
            break;
          case 'W':
            this.currentPosition.x--;
            break;
          case 'E':
            this.currentPosition.x++;
            break;
          case 'S':
            this.currentPosition.y++;
            break;
          }
      if (this.currentPosition.x < 0) this.currentPosition.x === this.grid.map[0].length;
      if (this.currentPosition.y < 0) this.currentPosition.y === this.grid.map.length;
  }
}
