import TPosition from '../Types/TPosition.type';
import TOrientation from '../Types/TOrientation.type';

export default class Grid {
  width: number;
  height: number;
  map: string[][];
  position: TPosition;

  constructor(grid: { width: number; height: number }, startPosition: TPosition = { x: 0, y: 0, orientation: 'N' }) {
    this.width = grid.width;
    this.height = grid.height;
    this.map = Array(this.height)
      .fill([])
      .map(() => Array(this.width).fill('0'));
    this.position = startPosition;
  }

  getOrientationChar(orientation: TOrientation) {
    switch (orientation) {
      case 'E':
        return '>';
      case 'N':
        return 'A';
      case 'S':
        return 'V';
      case 'W':
        return '<';
    }
  }

  print(map: string[][] = this.map, currentPosition: TPosition = this.position) {
    let mapString = '';
    const headerNumbers = Array(this.width)
      .fill('')
      .map((_, index) => `${index}`);
    console.log('\\', headerNumbers.join(' '));
    for (const [indexY, line] of map.entries()) {
      mapString += `${indexY}|`;
      for (const [indexX, column] of line.entries()) {
        if (indexX === currentPosition.x && indexY === currentPosition.y) {
          mapString += `${this.getOrientationChar(currentPosition.orientation)}|`;
        } else {
          mapString += `${column}|`;
        }
      }
      mapString += '\n';
    }
    console.log(mapString);
  }
}
