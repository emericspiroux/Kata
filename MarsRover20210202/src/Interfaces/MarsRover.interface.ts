import Grid from '../classes/Grid';

export default interface IMarsRover {
  execute(command: string): string;
}
